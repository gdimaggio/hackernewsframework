//
//  HNViewController.swift
//  HackerNews
//
//  Created by Gianluca Di maggio on 2/9/17.
//  Copyright © 2017 dima. All rights reserved.
//

import UIKit
import SafariServices

class HNViewController: UIViewController {

    // MARK: - Private properties

    @IBOutlet fileprivate weak var tableView: UITableView!
    fileprivate weak var activityIndicator: UIActivityIndicatorView!

    fileprivate let newsIdsUrl = URL(string: "https://hacker-news.firebaseio.com/v0/topstories.json")
    fileprivate let newsBaseUrl = URL(string: "https://hacker-news.firebaseio.com/v0/item")

    fileprivate var newsIds: [Int] = []
    fileprivate var news: [Int:HNNews] = [:] // Just for caching

    fileprivate let kMaxNewsCount = 100
    fileprivate let newsDateFormatter = DateFormatter()

    // MARK: - Private Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Basic tableview setup
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.newsDateFormatter.dateFormat = "MMM d, h:mm a"

        // Add an activity indicator to notify that news are downloading
        let activityIndicator = UIActivityIndicatorView()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: activityIndicator)
        self.activityIndicator = activityIndicator
        self.activityIndicator.hidesWhenStopped = true

        self.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let row = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: row, animated: true)
        }
    }

    /*
     * When reloading the table data, we only perform one network call
     * to retrieve the list of IDs associated to the top stories.
     * Each cell will be responsible to actually download a specific News
     */
    fileprivate func reloadData() {

        self.activityIndicator.startAnimating()
        self.downloadNewsIds { (data) in
            self.activityIndicator.stopAnimating()

            guard let ids = data, ids.count > 0 else { return }

            let maxNewsCount = ids.count >= self.kMaxNewsCount ? self.kMaxNewsCount : ids.count

            self.newsIds = Array(ids[0..<maxNewsCount])
            self.tableView.reloadData()
        }

    }

    // MARK: - Actions

    @IBAction private func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }

}

extension HNViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsIds.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: HNNewsTableViewCell.cellIdentifier,
                                                      for: indexPath) as! HNNewsTableViewCell

        let newsId = self.newsIds[indexPath.row]

        // Check if the news has downloaded/parsed and cached already
        if let news = self.news[newsId] {
            self.styleCell(cell, with: news)
        }
        // Otherwise perform a newtwork call and then cache the news
        else {
            DispatchQueue.global().async {
                self.downloadNews(for: newsId, completion: { (news) in
                    self.news[newsId] = news
                    DispatchQueue.main.async {
                        if let visibleCell = tableView.cellForRow(at: indexPath) as? HNNewsTableViewCell,
                            let news = news {
                            self.styleCell(visibleCell, with: news)
                        }
                    }
                })
            }
        }

        return cell
    }


    /*
     * Style the cell with the News info (title, author, date, etc..), then fade it in
     */
    private func styleCell(_ cell: HNNewsTableViewCell, with news: HNNews) {
        cell.titleLabel.text = news.title.uppercased()
        cell.authorLabel.text = news.author
        cell.scoreLabel.text = "\(news.score)"
        cell.dateLabel.text = self.newsDateFormatter.string(from: Date(timeIntervalSince1970: news.timestamp))

        UIView.animate(withDuration: 0.2, animations: { 
            cell.overlayView.alpha = 0
        }) { (completed) in
            cell.overlayView.isHidden = true
        }
    }

    // We use SafariViewController to display a specific news
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newsId = self.newsIds[indexPath.row]
        guard let news = self.news[newsId] else { return }

        if #available(iOS 9.0, *) {
            let safariController = SFSafariViewController(url: news.url)
            self.present(safariController, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            UIApplication.shared.openURL(news.url)
        }
    }

    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let newsId = self.newsIds[indexPath.row]
        guard let _ = self.news[newsId] else {
            return false
        }

        return true
    }

}

// Networking logic
extension HNViewController {

    /* 
     * Download a list of IDs corresponding to the Top News for today
     */
    fileprivate func downloadNewsIds(completion: @escaping ([Int]?) -> Void) {
        let task = URLSession.shared.dataTask(with: URLRequest(url: self.newsIdsUrl!)) { (data, response, error) in
            // An error occurred with the request
            if let error = error {
                print("An error occurred: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }

            // Check if any data has been received at all
            guard let data = data else {
                print("No data has been received")
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }

            // Turn the data into an array
            guard let parsedData = try? JSONSerialization.jsonObject(with: data, options: []) as? [Int] else {
                print("Json serialization failed")
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }

            DispatchQueue.main.async {
                print("Downloaded \(parsedData?.count) news IDs")
                completion(parsedData)
            }
        }
        task.resume()
    }

    /*
     * Given a News Id, grab the entire News json from the Firebase endpoint
     */
    fileprivate func downloadNews(for id:Int, completion: @escaping (HNNews?)->Void) {
        let newsUrl = self.newsBaseUrl!.appendingPathComponent("\(id)").appendingPathExtension("json")
        print(newsUrl)
        let task = URLSession.shared.dataTask(with: URLRequest(url: newsUrl)) { (data, response, error) in

            // An error occurred with the request
            if let error = error {
                print("An error occurred: \(error.localizedDescription)")
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }

            // Check if any data has been received at all
            guard let data = data else {
                print("No data has been received")
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }

            // Turn the data into an array
            guard let parsedData = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any],
                let news = self.parseNewsJson(parsedData) else {
                print("Json serialization failed")
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }

            DispatchQueue.main.async {
//                print("Downloaded news with ID: \(news.uuid)")
                completion(news)
            }

        }
        task.resume()
    }

    /* 
     * Parse the JSON received from the Firebase endpoint into a News object
     */
    fileprivate func parseNewsJson(_ json: [String:Any]?) -> HNNews? {
        guard let json = json, let id = json["id"] as? Int, let title = json["title"] as? String,
            let author = json["by"] as? String, let score = json["score"] as? Int, let time = json["time"] as? Int,
            let urlString = json["url"] as? String, let url = URL(string: urlString) else {
            return nil
        }

        return HNNews(uuid: id, title: title, author: author, score: score, timestamp: TimeInterval(time), url: url)
    }
}
