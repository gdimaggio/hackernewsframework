//
//  HackerNews.swift
//  HackerNews
//
//  Created by Gianluca Di maggio on 2/10/17.
//  Copyright © 2017 dima. All rights reserved.
//

import UIKit

public class HackerNews: NSObject {

    static var token: String?

    public static func initSDK() -> Void {

    }

    public static func showHackerNews() -> Void {
        guard let rootViewController = UIApplication.topViewController() else { return }

        let storyboard = UIStoryboard(name: "HackerNews", bundle: Bundle(for: self))
        if let controller = storyboard.instantiateInitialViewController() {
            rootViewController.present(controller, animated: true, completion: nil)
        }
    }

}
