//
//  ViewController.swift
//  HackerNewsExample
//
//  Created by Gianluca Di maggio on 2/9/17.
//  Copyright © 2017 dima. All rights reserved.
//

import UIKit
import HackerNews

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Action

    @IBAction private func showNewsFeed() {
        HackerNews.showHackerNews()
    }

}

