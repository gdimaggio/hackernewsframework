# README #

## Framework Installation ##

- Open the HackerNewsSwift project and build it for your selector platform - Simulator or Generic iOS Device.
- Grab the .framework from the Product folder and drop it into your XCode project.
- Select your Target and make sure to add the HackerNews.framework to the *Linked Frameworks and Libraries* and *Embedded Binaries* sections.

## Usage ##

To use the HackerNews framework, import it at the top of your source file first:

```
import HackerNews
```

Then call on the supported methods as follows:

```
HackerNews.showHackerNews() 
```